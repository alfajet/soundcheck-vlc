# -*- coding: utf-8 -*-

from soundcheck_vlc.utils import parseinput

import unittest


class BasicTestSuite(unittest.TestCase):
    """Basic test cases."""

    def test_parse_valid_hashtag(self):
        self.assertEqual(parseinput.parse_hashtag('soundcheck'),(parseinput.ParseStatus.PASS, 'soundcheck') )

    def test_parse_partial_valid_hashtag(self):
        self.assertEqual(parseinput.parse_hashtag('#soundcheck abc'),(parseinput.ParseStatus.PARTIAL_PASS, 'soundcheck') )

    def test_parse_invalid_hashtag(self):
        self.assertEqual(parseinput.parse_hashtag('#?<>{'),(parseinput.ParseStatus.FAIL, None) )


if __name__ == '__main__':
    unittest.main()